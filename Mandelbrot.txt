Imports System.Threading
''' <summary>
''' What is the mandelbrot set?
''' The Mandelbrot set is the amount of complex numbers c, from which it mathematical series
''' z0 = 0
''' zn+1 = zn^2+c
''' is smaller then 2. 
''' 
''' The larger the iteration maximum, the more precise the mandelbrot picture will be. 
''' All the values inside the mandelbrot set will be represented by the inner figure(white).
''' 
''' Features of the Program: 
'''     Calculating, drawing, zooming, saving the Mandelbrot set as jpeg or a gif
'''     multithreading, choice of the minimun and maximum x -and ycoordinates, choice of the 
'''     iterationdepth  
''' </summary>
Public Class Form1
    'minimum x coordinate for the calculation of the mandelbrot set
    Dim xMin As Decimal = 0
    'maximum x coordinate for the calculation of the mandelbrot set
    Dim xMax As Decimal = 0
    'minimum y coordinate for the calculation of the mandelbrot set
    Dim yMin As Decimal = 0
    'maximum y coordinate for the calculation of the mandelbrot set
    Dim yMax As Decimal = 0

    'width of the picturebox "pctMandelbrot"
    Dim wwidth As Integer = 0
    'height of the picturebox "pctMandelbrot"
    Dim hheight As Integer = 0

    ' Bitmap on which the mandelbrot set will get drawn on
    Dim Cloc_bmap As Drawing.Bitmap = Nothing
    ' drawing surface of the bitmap(Cloc_bmap)
    Dim Cloc_Graphics As Graphics = Nothing

    'the step size of the x axis of every Pixel
    Dim CLoc_stepsx As Decimal = 0
    'the Step size Of the y axis Of every Pixel
    Dim CLoc_stepsy As Decimal = 0

    'The scaling factor will scale the xmin to xmax and ymin to ymax coordinate system, to the picturebox coordinates
    'the scaling factor of x
    Dim CLoc_scaleX As Decimal = 0
    'the scaling factor of y
    Dim CLoc_scaleY As Decimal = 0

    'Checks if the mandelbrot set is already drawn
    Dim CLoc_check As Boolean = False

    'references to a function, which is refreshing the mandelbrot set picture
    Delegate Sub DEL_Refresh()
    'references to a function, which enables the upper menues and buttons 
    Delegate Sub DEL_PnlTrue()
    'references to a function, which draws a color depending on the pixel coordinates and its mathematical series
    Delegate Sub DEL_Draw()

    ''' <summary>
    ''' 
    ''' </summary>
    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.

    End Sub

    ''' <summary>
    ''' Starts the calculation of the Mandelbrot set via the function "SU_Start_Calc()".
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_start.Click
        SU_Start_Calc()
        CLoc_check = True
    End Sub

    ''' <summary>
    ''' The function handles the event "pctMandelbrot.Click"
    ''' calls the function "setZoomValues" which allows the user to zoom in to the mandelbrot picture
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pctMandelbrot_Click(sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pctMandelbrot.Click
        If CLoc_check = True Then
            setZoomValues(e.Location.X, e.Location.Y)
        End If
    End Sub

    ''' <summary>
    ''' Sets Zoom Level based on given coordinate. And writes the calculated values inside the textboxes(txtxmin, etc.)
    ''' </summary>
    ''' <param name="trans_location_x">Based on Picturebox X coordinate</param>
    ''' <param name="trans_location_y">Based on Picturebox Y coordinate</param>
    Private Sub setZoomValues(ByVal trans_location_x As Integer, ByVal trans_location_y As Integer)
        'Converts the mousepositon coordinates, to values from xmin to xmax and ymin to ymax
        'Mouseposition times the difference between xmin to xmax or ymin to ymax, divided by the width or height of the picturebox
        '"pctMandelbrot" -1
        Dim xz As Decimal = ((trans_location_x) * (xMax - xMin) / (pctMandelbrot.Width))
        Dim yz As Decimal = ((trans_location_y) * (yMax - yMin) / (pctMandelbrot.Height))
        'diffx and diffy are the differences between xmin to xmax or ymin to ymax
        Dim diffx As Decimal = xMax - xMin
        Dim diffy As Decimal = yMax - yMin
        txtxMin.Text = xMin + xz - (diffx / 10)
        txtxMax.Text = xMin + xz + (diffx / 10)
        txtyMin.Text = yMin + yz - (diffy / 10)
        txtyMax.Text = yMin + yz + (diffy / 10)
    End Sub

    ''' <summary>
    ''' The function declares and calculates relevant values for the mandelbrot set. It also starts the threads to calculate the mandelbrot set
    ''' </summary>
    Private Sub SU_Start_Calc()
        Try
            Dim Loc_ThreadCount As Integer = CInt(cb_threads.SelectedItem.ToString)
            If (Loc_ThreadCount > 0) Then
                p_menu.Enabled = False

                SU_DeclareXandYValues()

                wwidth = pctMandelbrot.Width - 1
                hheight = pctMandelbrot.Height - 1

                Cloc_bmap = Nothing
                Cloc_bmap = New Drawing.Bitmap(wwidth + 1, hheight + 1)

                'will be the step size of the x axis of every Pixel
                CLoc_stepsx = (xMax - xMin) / Cloc_bmap.Width
                'will be the step size of the y axis of every Pixel.
                CLoc_stepsy = (yMax - yMin) / Cloc_bmap.Height

                'the scaling factors will scale the x point of the coordinate, from the coordinate system between xmin, ymin, xmax and ymax
                'and will scale/transform it to a pixel
                'the scaling factor of x
                CLoc_scaleX = (wwidth) / (xMax - xMin)
                'the scaling factor of y
                CLoc_scaleY = (hheight) / (yMax - yMin)

                Cloc_Graphics = Nothing
                Cloc_Graphics = Graphics.FromImage(Cloc_bmap)

                Dim Loc_maxIterationDepth = CInt(txt_IterationDepth.Text)
                Cloc_Graphics.FillRectangle(Brushes.Black, 0, 0, pctMandelbrot.Width, pctMandelbrot.Height)

                pctMandelbrot.Image = Cloc_bmap

                Dim Loc_yMin As Decimal = 0
                Dim Loc_yMax As Decimal = 0
                ' Difference between ymax and ymin / the amount of threads the user selected 
                Dim Loc_ry As Decimal = (yMax - yMin) / Loc_ThreadCount
                Dim Loc_counter As Integer = 1

                ' Different case depending on selected thread count
                Select Case Loc_ThreadCount
                    Case 1
                        Loc_yMin = yMin
                        Loc_yMax = yMax
                    Case 2
                        Loc_yMin = yMin
                        Loc_yMax = yMax
                    Case 4
                        Loc_yMax = yMin
                        Loc_yMin = Loc_yMax
                    Case 8
                        Loc_yMin = yMin
                        Loc_yMax = yMax

                End Select

                'Depending on the wished thread count, it will run the wished amount of threads
                For t As Integer = 1 To Loc_ThreadCount
                    If Loc_counter = 1 Then
                        'This will be a parameter, which will be equal to ymin in the calculation
                        Dim Loc_yminimum As Decimal = Loc_yMin
                        'This will be a parameter, which will be equal to ymax in the calculation
                        Dim Loc_ymaximum As Decimal = Loc_yminimum + Loc_ry
                        Dim th_CMBV = New Thread(Sub()
                                                     SU_RunThroughEachCoordinate(Loc_maxIterationDepth, xMin, xMax, Loc_yminimum, Loc_ymaximum)
                                                 End Sub)
                        th_CMBV.Start()
                        Loc_counter = 2
                        Loc_yMin = yMax
                        Loc_yMax = yMin + Loc_ry
                    Else
                        Dim Loc_ymini As Decimal = Loc_yMax
                        'This will be a parameter, which will be equal to ymin in the calculation
                        Dim Loc_ymaxi As Decimal = Loc_ymini + Loc_ry
                        'This will be a parameter, which will be equal to ymax in the calculation
                        Dim th_CMBV = New Thread(Sub()
                                                     SU_RunThroughEachCoordinate(Loc_maxIterationDepth, xMin, xMax, Loc_ymini, Loc_ymaxi)
                                                 End Sub)
                        th_CMBV.Start()
                        Loc_counter = 2
                        Loc_yMax = Loc_ymaxi
                    End If
                Next
            End If


        Catch ex As Exception
        Finally

        End Try
    End Sub

    ''' <summary>
    ''' Declares xmin, xmax, ymin, ymax values
    ''' </summary>
    Private Sub SU_DeclareXandYValues()
        xMin = CDec(txtxMin.Text)
        xMax = CDec(txtxMax.Text)
        yMin = CDec(txtyMin.Text)
        yMax = CDec(txtyMax.Text)
    End Sub

    ''' <summary>
    ''' The function runs through each coordinate until every coordinate was beeing run through. It also refreshes the picturebox "pctMandelbrot"
    ''' </summary>
    ''' <param name="trans_maxIeration"></param>
    ''' <param name="trans_xMin">xmin</param>
    ''' <param name="trans_xMax">xmax</param>
    ''' <param name="trans_yMin">ymin</param>
    ''' <param name="trans_yMax">ymax</param>
    Private Sub SU_RunThroughEachCoordinate(ByVal trans_maxIeration As Decimal, ByVal trans_xMin As Decimal, ByVal trans_xMax As Decimal, ByVal trans_yMin As Decimal, ByVal trans_yMax As Decimal)

        Dim xminOld As Decimal
        Dim x1 As Decimal
        Dim y1 As Decimal

        Dim xn As Decimal
        Dim yn As Decimal

        x1 = trans_xMin
        y1 = trans_yMax
        xminOld = trans_xMin

        Dim Loc_del As New DEL_Refresh(AddressOf SU_Refresh_Picture)

        While y1 > trans_yMin
            While x1 < trans_xMax
                calculationOfMandelbrotValues(0, trans_maxIeration, x1, y1, xn, yn)
                x1 += CLoc_stepsx

            End While
            pctMandelbrot.Invoke(Loc_del)
            Console.WriteLine("Finished X (" + CStr(x1) + "/" + CStr(y1) & ")")
            x1 = xminOld
            y1 -= CLoc_stepsy
        End While
        Dim Loc_delpnl As New DEL_PnlTrue(AddressOf SU_Enable_Panel)
        p_menu.Invoke(Loc_delpnl)
    End Sub

    ''' <summary>
    ''' The function is the calculation of the mandelbrot set
    ''' It calculates each coordinate, lets it run through the mandelbrot equation, checks if its in the mandelbrot set or not
    ''' and calls the function "SU_drawPixel" to draw the mandelbrot set picture
    ''' </summary>
    ''' <param name="trans_iterNumber"></param>
    ''' <param name="trans_maxIterNumber">maximum iteration</param>
    ''' <param name="x0"></param>
    ''' <param name="y0"></param>
    ''' <param name="xnz">temporary storage of xn</param>
    ''' <param name="ynz">temporary storage of yn</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function calculationOfMandelbrotValues(trans_iterNumber As Integer, trans_maxIterNumber As Integer, x0 As Decimal,
                                                   y0 As Decimal, xnz As Decimal, ynz As Decimal) As Decimal


        If trans_iterNumber = 0 Then
            calculationOfMandelbrotValues = calculationOfMandelbrotValues(trans_iterNumber + 1, trans_maxIterNumber, x0, y0, x0, y0)

        ElseIf trans_iterNumber <> 0 Then
            'xnz = old xn(yn from the last calc)
            'ynz = old yn(yn from the last calc)
            'x0 = new x0 coordinate
            'y0 = new y0 coordinate
            Dim xnz0 As Decimal = (xnz) ^ 2 - (ynz) ^ 2 + (x0)
            Dim ynz0 As Decimal = 2 * (xnz) * (ynz) + (y0)
            Dim r As Decimal = Math.Sqrt((xnz0) ^ 2 + (ynz0) ^ 2)

            If r > 2 Then
                If InvokeRequired Then
                    'CLoc_scale X is the scale factor of x(xmax - xmin)
                    'CLoc_scale Y is the scale factor of y(ymax - ymin)
                    'The calculations results, will be the values of the pixels coordinate in the bitmap
                    SU_drawPixel((-(xMin * CLoc_scaleX)) + (x0) * CLoc_scaleX,
                    (-(yMin * CLoc_scaleY)) + (y0) * CLoc_scaleY, trans_iterNumber, trans_maxIterNumber, 0)
                Else
                    'CLoc_scale X is the scale factor of x(xmax - xmin)
                    'CLoc_scale Y is the scale factor of y(ymax - ymin)
                    'The calculations results, will be the values of the pixels coordinate in the bitmap
                    Dim t As New Task(Sub() Me.SU_drawPixel((-(xMin * CLoc_scaleX)) + (x0) * CLoc_scaleX,
                        (-(yMin * CLoc_scaleY)) + (y0) * CLoc_scaleY, trans_iterNumber, trans_maxIterNumber, 0))
                End If
                calculationOfMandelbrotValues = trans_maxIterNumber

            Else
                If trans_iterNumber >= trans_maxIterNumber Then
                    If InvokeRequired Then
                        'CLoc_scale X is the scale factor of x(xmax - xmin)
                        'CLoc_scale Y is the scale factor of y(ymax - ymin)
                        'The calculations results, will be the values of the pixels coordinate in the bitmap
                        SU_drawPixel((-(xMin * CLoc_scaleX)) + (x0) * CLoc_scaleX,
                    (-(yMin * CLoc_scaleY)) + (y0) * CLoc_scaleY, trans_iterNumber, trans_maxIterNumber, 1)
                    Else
                        'CLoc_scale X is the scale factor of x(xmax - xmin)
                        'CLoc_scale Y is the scale factor of y(ymax - ymin)
                        'The calculations results, will be the values of the pixels coordinate in the bitmap
                        Dim t As New Task(Sub() Me.SU_drawPixel((-(xMin * CLoc_scaleX)) + (x0) * CLoc_scaleX,
                        (-(yMin * CLoc_scaleY)) + (y0) * CLoc_scaleY, trans_iterNumber, trans_maxIterNumber, 1))
                    End If
                    calculationOfMandelbrotValues = trans_maxIterNumber
                Else
                    calculationOfMandelbrotValues = calculationOfMandelbrotValues(trans_iterNumber + 1, trans_maxIterNumber, x0, y0, xnz0, ynz0)
                End If
            End If
        End If
    End Function

    ''' <summary>
    ''' the function draws a color depending on the coordinate mathimatical series and its iterationdepth
    ''' </summary>
    ''' <param name="x">x-coordinate</param>
    ''' <param name="y">y-coordinate</param>
    ''' <param name="transIter">current iteration</param>
    ''' <param name="transIterMax">max iteration</param>
    Private Sub SU_drawPixel(x As Decimal, y As Decimal, transIter As Decimal, transIterMax As Decimal, ByVal trans_mandelbrot As Decimal)

        Dim Loc_Color_R As Integer = 0
        Dim Loc_Color_G As Integer = 0
        Dim Loc_Color_B As Integer = 0
        'Set the color of the pixels from the bitmap, depending on their iterationdepth
        Dim Loc_Color_Value As Integer = ((255) / (transIterMax) * (transIter))
        If InvokeRequired Then
            If transIter = transIterMax Then
                Loc_Color_R = 255
                Loc_Color_G = 255
                Loc_Color_B = 255

            ElseIf trans_mandelbrot = 0 Then
                Loc_Color_G = Loc_Color_Value
            ElseIf transIter < (transIterMax / 3) Then
                Loc_Color_R = Loc_Color_Value
                Loc_Color_G = Loc_Color_Value
                Loc_Color_B = Loc_Color_Value
            ElseIf transIter < (transIterMax / 1.5) Then
                Loc_Color_R = Loc_Color_Value
                Loc_Color_G = Loc_Color_Value
                Loc_Color_R = Loc_Color_Value
            ElseIf transIter > (transIterMax / 1.5) Then
            End If

            If Me.InvokeRequired Then
                Dim loc_draws As New DEL_Draw(Sub()
                                                  Cloc_bmap.SetPixel(x, y, Color.FromArgb(255, Loc_Color_R, Loc_Color_G, Loc_Color_B))
                                              End Sub)
                Me.Invoke(loc_draws)
            Else
                Cloc_bmap.SetPixel(x, y, Color.FromArgb(255, Loc_Color_R, Loc_Color_G, Loc_Color_B))
            End If
        End If
    End Sub

    ''' <summary>
    ''' The function refreshes the picture of the picturebox "pctMandelbrot"
    ''' </summary>
    Private Sub SU_Refresh_Picture()
        pctMandelbrot.Refresh()
    End Sub

    ''' <summary>
    ''' The function enables the uppermenu(save, reset etc.) for the user
    ''' </summary>
    Private Sub SU_Enable_Panel()
        p_menu.Enabled = True
    End Sub

    ''' <summary>
    ''' The function handles the event "btnReset.Click"
    ''' Calls the function "saveImg()" when "btnsave" is clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btnsave.Click
        saveImg()
    End Sub

    ''' <summary>
    ''' The function allows to save the picture of the mandelbrot set in JPEG or GIF format
    ''' </summary>
    Private Sub saveImg()
        SaveFileDialog1.Filter = "JPEG Image|*.jpg|Gif Image|*.gif"
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            If SaveFileDialog1.FilterIndex = 1 Then
                pctMandelbrot.Image.Save(SaveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Jpeg)
            ElseIf SaveFileDialog1.FilterIndex = 1 Then
                pctMandelbrot.Image.Save(SaveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If
    End Sub

    ''' <summary>
    ''' The function handles the event "btnReset.Click"
    ''' Calls the function resetValues when "btnsave" is clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        resetValues()
    End Sub

    ''' <summary>
    ''' The functions resets the menu labels(txtMin etc.) to their standard values
    ''' </summary>
    Private Sub resetValues()
        txtxMin.Text = -2
        txtxMax.Text = 1
        txtyMin.Text = -1
        txtyMax.Text = 1
        cb_threads.Text = 8
    End Sub

End Class
